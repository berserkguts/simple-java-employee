package com.citi.training.employee.menu;

public interface InputScanner 
{
	String getString(String propmtMsg);
	public double getDouble(String promptMsg);
	public int getInt(String promptMsg);
}
